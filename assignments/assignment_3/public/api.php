<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use \Infrastructure\Web\Kernel\HttpFramework;

$request = Request::createFromGlobals();
$routes = include __DIR__ . '/../src/Infrastructure/Web/Application/routes.php';

$context = new RequestContext();
$matcher = new UrlMatcher($routes, $context);

$controllerResolver = new ControllerResolver();
$argumentResolver = new ArgumentResolver();

$httpFramework = new HttpFramework(
    $matcher,
    $controllerResolver,
    $argumentResolver
);
$response = $httpFramework->handle($request);

$response->send();

