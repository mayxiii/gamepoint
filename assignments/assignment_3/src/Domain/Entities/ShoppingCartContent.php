<?php

namespace Domain\Entities;

/**
 * @Entity
 * @Table(name="shoppingCartContent")
 */
class ShoppingCartContent
{

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var int
     *
     * @Column(type="integer", name="cartID")
     */
    private $cartId;

    /**
     * @var int
     *
     * @Column(type="integer", name="productID")
     */
    private $productId;

    /**
     * @var int
     *
     * @Column(type="integer", name="quantity")
     */
    private $quantity;

    /**
     * @OneToOne(targetEntity="Product")
     * @JoinColumn(name="productId", referencedColumnName="id", nullable=false)
     */
    private $product;

    /**
     * @ManyToOne(targetEntity="ShoppingCart", inversedBy="shoppingCartContent")
     * @JoinColumn(name="cartID", referencedColumnName="id", nullable=false)
     */
    private $shoppingCart;

    public function __construct(int $id, int $cartId, int $productId, int $quantity)
    {
        $this->id = $id;
        $this->cartId = $cartId;
        $this->productId = $productId;
        $this->quantity = $quantity;
    }

}