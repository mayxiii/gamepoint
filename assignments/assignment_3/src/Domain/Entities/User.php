<?php

namespace Domain\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity
 * @Table(name="user")
 */
class User
{

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", name="firstname")
     */
    private $firstName;

    /**
     * @var string
     *
     * @Column(type="string", name="lastname")
     */
    private $lastName;

    /**
     * @OneToMany(targetEntity="Payment", mappedBy="user")
     */
    private $payments;

    public function __construct(int $id, string $firstName, string $lastName)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;

        $this->payments = new ArrayCollection();
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

}