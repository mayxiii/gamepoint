<?php

namespace Infrastructure\Web\Application\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Infrastructure\Queries\MySQL\ShoppingCart\MySQLOrderDetailsQuery;
use Application\UseCases\GetOrderDetailsUseCase;
use Application\UseCases\ResultFormatters\SimpleUseCaseListResultFormatter;

class OrderController
{
    public function getOrderDetailsAction(Request $request): Response
    {
        $requestData['max_num'] = $request->get('max_num', 5) + 1;
        $requestData['next_offset'] = $request->get('next_offset', 0);


        $entityManager = \Application\DocumentManagerFactory::getEntityManager(
            'pdo_mysql',
            'localhost',
            'root',
            'gamepointtrialzday',
            'gamepoint',
            'src/Domain/Entities',
            'cache/MySQL'
        );

        $getOrderDetailsUseCase = new GetOrderDetailsUseCase(
            new MySQLOrderDetailsQuery($entityManager),
            new SimpleUseCaseListResultFormatter(),
            $requestData
        );
        $data = $getOrderDetailsUseCase->invoke();


        return new Response(json_encode($data));
    }
}