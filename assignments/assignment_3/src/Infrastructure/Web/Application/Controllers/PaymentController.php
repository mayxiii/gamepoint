<?php

namespace Infrastructure\Web\Application\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Infrastructure\Queries\MySQL\Payment\MySQLPaymentsByUserNameQuery;
use Application\UseCases\GetPaymentsByUserNameUseCase;
use Application\UseCases\ResultFormatters\SimpleUseCaseListResultFormatter;

class PaymentController
{
    public function getPaymentsForUserAction(Request $request): Response
    {
        $requestData['user_name'] = $request->get('user_name', 'Henk');
        $requestData['max_num'] = $request->get('max_num', 5) + 1;
        $requestData['next_offset'] = $request->get('next_offset', 0);


        $entityManager = \Application\DocumentManagerFactory::getEntityManager(
            'pdo_mysql',
            'localhost',
            'root',
            'gamepointtrialzday',
            'gamepoint',
            'src/Domain/Entities',
            'cache/MySQL'
        );

        $getPaymentsByUserNameUseCase = new GetPaymentsByUserNameUseCase(
            new MySQLPaymentsByUserNameQuery($entityManager),
            new SimpleUseCaseListResultFormatter(),
            $requestData
        );

        $data = $getPaymentsByUserNameUseCase->invoke();


        return new Response(json_encode($data));
    }
}