<?php

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();
$routes->add(
    'payments_for_user',
    new Routing\Route(
        '/api/payments/search',
        array(
            '_controller' => 'Infrastructure\Web\Application\Controllers\PaymentController::getPaymentsForUserAction',
        )
    )
);
$routes->add(
    'order_details',
    new Routing\Route(
        '/api/order/details',
        array(
            '_controller' => 'Infrastructure\Web\Application\Controllers\OrderController::getOrderDetailsAction',
        )
    )
);

return $routes;
