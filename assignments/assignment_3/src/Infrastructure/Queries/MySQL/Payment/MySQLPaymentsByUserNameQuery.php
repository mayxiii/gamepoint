<?php

namespace Infrastructure\Queries\MySQL\Payment;

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Application\Queries\Payment\PaymentsByUserNameQuery;
use Domain\Entities\Payment;

class MySQLPaymentsByUserNameQuery implements PaymentsByUserNameQuery
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ObjectRepository
     */
    protected $repository;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Payment::class);
    }

    public function execute(string $userName, int $limit, int $offset): array
    {
        $offset = ($offset < 0 ? 0 : $offset);

        $queryBuilder = $this->repository->createQueryBuilder('payment')
            ->leftJoin('payment.user', 'user')

            ->select([
                'payment.id as payment_id',
                'user.id as user_id',
                'user.firstName as user_firstname',
                'user.lastName as user_lastname',
                'payment.totalPrice as payment_totalPrice',
                'payment.status as payment_status',
                'payment.methodId as payment_methodID'
            ]);

        if($limit > 0) {
            $queryBuilder->setMaxResults($limit)
                ->setFirstResult($offset)
                ->orderBy('user_id')->addOrderBy('payment_id');
        }

        return $queryBuilder->andWhere('user.firstName = :userName')
            ->setParameter('userName', $userName)
            ->getQuery()
            ->getResult();
    }

}