<?php

namespace Application\UseCases;

interface UseCase
{

    public function invoke(): array;
}