<?php

namespace Application\UseCases;

use Application\Queries\ShoppingCart\OrderDetailsQuery;

class GetOrderDetailsUseCase implements UseCase
{

    protected $query;

    protected $listResultFormatter;

    protected $requestData;

    public function __construct(OrderDetailsQuery $query, UseCaseListResultFormatter $listResultFormatter, array $requestData)
    {
        $this->query = $query;
        $this->requestData = $requestData;
        $this->listResultFormatter = $listResultFormatter;
    }

    public function invoke(): array
    {
        $limit = $this->requestData['max_num'];
        $offset = $this->requestData['next_offset'];

        $data = $this->query->execute($limit, $offset);

        return $this->listResultFormatter->format($data, $limit, $offset);
    }



}