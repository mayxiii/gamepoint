<?php

namespace Application\UseCases\ResultFormatters;

use Application\UseCases\UseCaseListResultFormatter;

class SimpleUseCaseListResultFormatter implements UseCaseListResultFormatter
{

    public function format(array $data, int $limit, int $offset): array
    {
        $realLimit = $limit - 1;
        $nextOffset = -1;
        if(count($data) == $limit){
            $nextOffset = ($offset<0 ? 0 : $offset) + $realLimit;
            array_pop($data);
        }

        return array(
            'data' => $data,
            'next_offset' => $nextOffset
        );
    }

}