<?php

namespace Application\UseCases;

interface UseCaseListResultFormatter{

    public function format(array $data, int $limit, int $offset): array;

}