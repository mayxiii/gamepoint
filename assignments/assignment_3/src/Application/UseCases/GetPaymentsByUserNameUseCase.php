<?php

namespace Application\UseCases;

use Application\Queries\Payment\PaymentsByUserNameQuery;
use Application\Exceptions\ApplicationException;

class GetPaymentsByUserNameUseCase implements UseCase
{

    protected $query;

    protected $listResultFormatter;

    protected $requestData;

    public function __construct(PaymentsByUserNameQuery $query, UseCaseListResultFormatter $listResultFormatter, array $requestData)
    {
        $this->query = $query;
        $this->requestData = $requestData;
        $this->listResultFormatter = $listResultFormatter;
    }

    public function invoke(): array
    {
        if(!$this->validateRequestData())
            throw new ApplicationException('User Name is not defined.');

        $userName = $this->requestData['user_name'];
        $limit = $this->requestData['max_num'];
        $offset = $this->requestData['next_offset'];

        $data = $this->query->execute($userName, $limit, $offset);

        return $this->listResultFormatter->format($data, $limit, $offset);
    }

    protected function validateRequestData(): bool
    {
        if(empty($this->requestData['user_name']))
            return false;

        return true;
    }
}