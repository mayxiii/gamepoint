<?php

namespace Application\Queries\Payment;

interface PaymentsByUserNameQuery
{

    public function execute(string $userName, int $limit, int $offset): array;

}