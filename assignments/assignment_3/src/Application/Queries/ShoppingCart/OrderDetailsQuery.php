<?php

namespace Application\Queries\ShoppingCart;

interface OrderDetailsQuery
{

    public function execute(int $limit, int $offset): array;

}