module.exports = {
    entry: "./js/app.js",
    output: {
        filename: "../public/js/bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.html/,
                loader: 'ractive-component-loader'
            }
        ]
    }
};
