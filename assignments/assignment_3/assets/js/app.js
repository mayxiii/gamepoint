$(function(){
    var Ractive = require('ractive');
    var PaymentsView = require('../views/payments.html');

    var ractive = new Ractive({
        el: '#payments-view',
        template: '#payments-view-template',
        data: {},
        components: {
            PaymentsView: PaymentsView
        }
    });
});
