<?php
/**
 * Assignment #3
 * Use the same code from assignment #2
 *
 * Create a web page which fetches the data without a refresh/pageload
 * The page should initially show a button with the text 'Fetch all payments'
 * - If you click on it, you should fetch the data in a JSON format. Result should look like the image below (see imgur.com URL)
 *
 * Minimal requirements:
 * - Use jQuery
 * - The fetched data should be in JSON format
 * - Loop through the data and show the output in a HTML table (so JSON shouldn't return HTML, but pure JSON)
 *
 * You're allowed to use other frontend/JS frameworks (i.e. Bootstrap, jQuery plugins)
 * 
 * Output should be similar to http://imgur.com/a/jUKm1
 */

if (version_compare(phpversion(), '7.1.0', '<')) {
    exit('PHP version is not high enough!. It must be at least 7.1.0');
}

echo '<a href="public/index.html">Assignment 3 solution</a>';


