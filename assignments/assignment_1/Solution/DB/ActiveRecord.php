<?php

class ActiveRecord{

    /**
     * @param $id
     * @return ActiveRecord
     * @throws ReflectionException
     */
    public static function getInstanceById($id): ActiveRecord{
        $data = static::retrieveRowById($id);
        return static::createInstanceFromArray($data);
    }

    /**
     * No real data fetch,
     * @param $id
     * @return array
     */
    protected static function retrieveRowById($id){
        //Some data fetching here ...
        $data = array(
            'id' => $id
        );
        return $data;
    }

    /**
     * Throws the exception if static::class doesn't exists. But it always exists :)
     * @param array $data
     * @return ActiveRecord
     * @throws ReflectionException
     */
    protected static function createInstanceFromArray(array $data): ActiveRecord{
        $reflection = new ReflectionClass(static::class);
        $instance = $reflection->newInstanceWithoutConstructor();
        $instance->setAttributes($data);
        return $instance;
    }

    /**
     * I assume that entity attribute names and keys in $values array matches
     * @param array $values
     */
    public function setAttributes(array $values){
        foreach($values as $fieldName => $fieldValue){
            $this->$fieldName = $fieldValue;
        }
    }

}