<?php

require_once 'Solution/DB/ActiveRecord.php';

class Payment extends ActiveRecord{

    /**
     * @var User
     */
    private $user;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var PaymentMethod
     */
    private $paymentMethod;

    public function __construct(User $user, Product $product, PaymentMethod $paymentMethod){
        $this->user = $user;
        $this->product  = $product;
        $this->paymentMethod = $paymentMethod;
    }

    public function getMethod(){
        return $this->paymentMethod;
    }

    public function getProduct(){
        return $this->product;
    }

    public function authorize(){
        //Some complex algorithm
        return array(
            'status' => 'authorized'
        );
    }

}