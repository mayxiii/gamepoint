<?php

class ProductFactory{

    private static $productNamesMapping = array(
        'vip' => 'VIP',
        'coins' => 'Coins',
    );

    public static function create($productKey): Product{
        $productName = self::mapProductName($productKey);

        if(self::productExists($productName)){
            return self::loadProduct($productName);
        }

        return self::loadDefaultProduct();
    }

    private static function mapProductName($productKey){
        if(!empty(self::$productNamesMapping[$productKey]))
            return self::$productNamesMapping[$productKey];

        return '';
    }

    private static function productExists($productName){
        $className = self::getProductClassName($productName);
        $classPath = self::getProductClassFilePath($productName);
        if(file_exists($classPath))
            require_once $classPath;
        return class_exists($className);
    }

    private static function getProductClassName($productName){
        return $productName . 'Product';
    }

    private static function getProductClassFilePath($productName){
        $className = self::getProductClassName($productName);
        return 'Solution/Entities/Products/'.$className.'.php';
    }

    private static function loadProduct($productName){
        $className = self::getProductClassName($productName);
        return new $className();
    }

    private static function loadDefaultProduct(){
        require_once 'Solution/Entities/Product.php';
        return new Product();
    }
}