<?php

require_once 'Solution/DB/ActiveRecord.php';

class Product extends ActiveRecord{

    private $price = 0;

    public function setPrice(int $price){
        $this->price = $price;
    }

    public function getPrice(): int{
        return $this->price;
    }

}