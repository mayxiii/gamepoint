<?php

require_once 'Solution/Entities/PaymentMethod.php';

class DefaultPaymentMethod extends PaymentMethod{

    public function __construct(){
    }

    public function setPaidPrice(int $paidPrice){
    }

    public function isApproved(): bool{
        //Is not approved. This is a Null Object
        return false;
    }

    public function paidPrice(): int{
        return 0;
    }

}