<?php

class PaymentMethodFactory{

    private static $paymentMethodNamesMapping = array(
        'paypal' => 'PayPal',
        'credit_card' => 'CreditCard',
    );

    public static function create($paymentMethodKey): PaymentMethod{
        $paymentMethodName = self::mapPaymentMethodName($paymentMethodKey);

        if(self::paymentMethodExists($paymentMethodName)){
            return self::loadPaymentMethod($paymentMethodName);
        }

        return self::loadDefaultPaymentMethod();
    }

    private static function mapPaymentMethodName($paymentMethodKey){
        if(!empty(self::$paymentMethodNamesMapping[$paymentMethodKey]))
            return self::$paymentMethodNamesMapping[$paymentMethodKey];

        return '';
    }

    private static function paymentMethodExists($paymentMethodName){
        $className = self::getPaymentClassName($paymentMethodName);
        $classPath = self::getPaymentClassFilePath($paymentMethodName);
        if(file_exists($classPath))
            require_once $classPath;
        return class_exists($className);
    }

    private static function getPaymentClassName($paymentMethodName){
        return $paymentMethodName . 'PaymentMethod';
    }

    private static function getPaymentClassFilePath($paymentMethodName){
        $className = self::getPaymentClassName($paymentMethodName);
        return 'Solution/Entities/PaymentMethods/'.$className.'.php';
    }

    private static function loadPaymentMethod($paymentMethodName){
        $className = self::getPaymentClassName($paymentMethodName);
        return new $className();
    }

    private static function loadDefaultPaymentMethod(){
        require_once 'Solution/Entities/PaymentMethod.php';
        return new PaymentMethod();
    }
}