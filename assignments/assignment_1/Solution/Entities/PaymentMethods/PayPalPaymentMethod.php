<?php

require_once 'Solution/Entities/PaymentMethod.php';

class PayPalPaymentMethod extends PaymentMethod{

    private $paidPrice = 0;

    public function __construct(){
    }

    public function setPaidPrice(int $paidPrice){
        $this->paidPrice = $paidPrice;
    }

    public function isApproved(): bool{
        return true;
    }

    public function paidPrice(): int{
        return $this->paidPrice;
    }

}