<?php
/**
 * Assignment #1
 * Build all needed classes to run this code succesfully
 * Keep in mind:
 * - It should be possible to pay with CreditCardMethod
 * - It should be possible to buy coins as a product
 */


if (version_compare(phpversion(), '7.1.0', '<')) {
    exit('PHP version is not high enough!. It must be at least 7.1.0');
}
chdir(dirname(__FILE__));
require_once 'Solution/Entities/User.php';
require_once 'Solution/Entities/Products/ProductFactory.php';
require_once 'Solution/Entities/PaymentMethods/PaymentMethodFactory.php';
require_once 'Solution/Entities/Payment.php';


$user = User::getInstanceById(1234);
$product = ProductFactory::create('vip');
$product->setPrice(100); // in cents

$paymentMethod = PaymentMethodFactory::create('paypal');

$payment = new Payment($user, $product, $paymentMethod);
$paymentMethod->setPaidPrice(100);


if($payment->getMethod()->isApproved() && $payment->getProduct()->getPrice() == $payment->getMethod()->paidPrice()) {
    var_dump($payment->authorize());
}