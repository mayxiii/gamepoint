<?php

namespace Infrastructure\Queries\MySQL\ShoppingCart;

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Domain\Entities\ShoppingCart;
use Application\Queries\ShoppingCart\OrderDetailsQuery;

class MySQLOrderDetailsQuery implements OrderDetailsQuery
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ObjectRepository
     */
    protected $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(ShoppingCart::class);
    }

    public function execute(): array{

        return $this->repository->createQueryBuilder('shoppingCart')
            ->leftJoin('shoppingCart.user', 'user')
            ->leftJoin('shoppingCart.payment', 'payment')
            ->leftJoin('payment.paymentMethod', 'paymentMethod')
            ->leftJoin('shoppingCart.shoppingCartContents', 'shoppingCartContents')
            ->leftJoin('shoppingCartContents.product', 'product')

            ->select([
                'user.firstName as firstname',
                'user.lastName as lastname',
                'payment.id as paymentid',
                'payment.totalPrice as price',
                'payment.status as status',
                'paymentMethod.name as methodname',
                'product.name as productname'
            ])

            ->getQuery()
            ->getArrayResult();

    }

}