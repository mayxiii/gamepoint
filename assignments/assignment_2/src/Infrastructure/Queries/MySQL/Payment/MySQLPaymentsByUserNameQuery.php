<?php

namespace Infrastructure\Queries\MySQL\Payment;

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Domain\Entities\Payment;
use Application\Queries\Payment\PaymentsByUserNameQuery;

class MySQLPaymentsByUserNameQuery implements PaymentsByUserNameQuery
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ObjectRepository
     */
    protected $repository;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Payment::class);
    }

    public function execute(string $userName): array
    {
        return $this->repository->createQueryBuilder('payment')
            ->leftJoin('payment.user', 'user')

            ->select([
                'payment.id as payment_id',
                'user.firstName as user_firstname',
                'user.lastName as user_lastname',
                'payment.totalPrice as payment_totalPrice',
                'payment.status as payment_status',
                'payment.methodId as payment_methodID'
            ])

            ->andWhere('user.firstName = :userName')
            ->setParameter('userName', $userName)
            ->getQuery()
            ->getResult();
    }

}