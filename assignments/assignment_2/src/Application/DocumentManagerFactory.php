<?php

namespace Application;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

//use Doctrine\ORM\EntityManager;
//use Doctrine\ORM\Configuration;

class DocumentManagerFactory
{
    const PROXY_DIR_NAME = 'Proxies';
    const PROXY_NAMESPACE = 'Proxies';

    /**
     * @param string $driverName
     * @param string $hostname$cache = new \Doctrine\Common\Cache\ApcCache;
     * @param string $username
     * @param string $password
     * @param string $database
     * @param string $entitiesPath
     * @param string $cachePath
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public static function getEntityManager(string $driverName, string $hostname, string $username, string $password, string $database, string $entitiesPath, string $cachePath): EntityManager
    {

        $connectionParams = array(
            'driver' => $driverName,
            'dbname' => $database,
            'user' => $username,
            'password' => $password,
            'host' => $hostname,
        );

        $config = Setup::createAnnotationMetadataConfiguration([$entitiesPath]);
        $config->setProxyDir($cachePath . '/' . self::PROXY_DIR_NAME);
        $config->setProxyNamespace(self::PROXY_NAMESPACE);
        return EntityManager::create($connectionParams, $config);
    }

}