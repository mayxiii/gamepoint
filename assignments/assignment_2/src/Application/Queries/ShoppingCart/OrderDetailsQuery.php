<?php

namespace Application\Queries\ShoppingCart;

interface OrderDetailsQuery
{

    public function execute(): array;

}