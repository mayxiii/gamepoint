<?php

namespace Application\Queries\Payment;

interface PaymentsByUserNameQuery
{

    public function execute(string $userName): array;

}