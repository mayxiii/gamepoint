<?php

namespace Application\UseCases;

use Application\Queries\Payment\PaymentsByUserNameQuery;
use Application\Exceptions\ApplicationException;

class GetPaymentsByUserNameUseCase implements UseCase
{

    protected $query;

    protected $requestData;

    public function __construct(PaymentsByUserNameQuery $query, array $requestData)
    {
        $this->query = $query;
        $this->requestData = $requestData;
    }

    public function invoke(): array
    {
        if(!$this->validateRequestData())
            throw new ApplicationException('User Name is not defined.');

        $userName = $this->requestData['user_name'];
        $resultData = $this->query->execute($userName);

        return $resultData;
    }

    protected function validateRequestData(): bool
    {
        if(empty($this->requestData['user_name']))
            return false;

        return true;
    }
}