<?php

namespace Application\UseCases;

use Application\Queries\ShoppingCart\OrderDetailsQuery;

class GetOrderDetailsUseCase implements UseCase
{

    protected $query;
    protected $requestData;

    public function __construct(OrderDetailsQuery $query, array $requestData)
    {
        $this->query = $query;
        $this->requestData = $requestData;
    }

    public function invoke(): array
    {
        $orderDetails = $this->query->execute();

        return $orderDetails;
    }



}