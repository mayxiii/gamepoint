<?php

namespace Domain\Entities;

/**
 * @Entity
 * @Table(name="shoppingCart")
 */
class ShoppingCart
{

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var int
     *
     * @Column(type="integer", name="userID")
     */
    private $userId;

    /**
     * @var int
     *
     * @Column(type="integer", name="paymentID")
     */
    private $paymentId;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="shoppingCart")
     * @JoinColumn(name="userID", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="Payment", inversedBy="shoppingCart")
     * @JoinColumn(name="paymentID", referencedColumnName="id", nullable=false)
     */
    private $payment;

    /**
     * @OneToMany(targetEntity="ShoppingCartContent", mappedBy="shoppingCart")
     */
    private $shoppingCartContents;

    public function __construct(int $id, int $userId, int $paymentId)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->paymentId = $paymentId;
    }

}