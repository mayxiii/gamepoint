<?php

namespace Domain\Entities;

/**
 * @Entity
 * @Table(name="payment")
 */
class Payment
{

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var int
     *
     * @Column(type="integer", name="userID")
     */
    private $userId;

    /**
     * @var string
     *
     * @Column(type="string", columnDefinition="ENUM('forwarded', 'pending', 'authorised')", name="status")
     */
    private $status;

    /**
     * @var int
     *
     * @Column(type="integer", name="totalPrice")
     */
    private $totalPrice;

    /**
     * @var int
     *
     * @Column(type="integer", name="methodID")
     */
    private $methodId;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="payment")
     * @JoinColumn(name="userID", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="PaymentMethod", inversedBy="payment")
     * @JoinColumn(name="methodID", referencedColumnName="id")
     */
    private $paymentMethod;

    /**
     * @OneToOne(targetEntity="ShoppingCart")
     * @JoinColumn(name="id", referencedColumnName="paymentId")
     */
    private $shoppingCart;


    public function __construct(int $id, int $userId, string $status, int $totalPrise, int $methodId)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->status = $status;
        $this->totalPrice = $totalPrise;
        $this->methodId = $methodId;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

}