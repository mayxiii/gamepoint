<?php

namespace Domain\Entities;

/**
 * @Entity
 * @Table(name="product")
 */
class Product
{

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", name="name")
     */
    private $name;

    public function __construct(int $id, int $name/*, string $price*/)
    {
        $this->id = $id;
        $this->name = $name;
//        $this->price = $price;
    }

}