<?php

namespace Domain\Entities;

/**
 * @Entity
 * @Table(name="method")
 */
class PaymentMethod
{

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", name="name")
     */
    private $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}