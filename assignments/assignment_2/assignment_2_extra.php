<?php
/**
 * Assignment #2 bonus
 * Extra tables (and reuse the tables from assignment #2)
 * - shoppingCart
 * - shoppingCartContent
 * - product
 * - method
 *
 * Output should be something like this:
 * <firstname> <lastname>       <paymentid>     <price>     <status>    <methodname>    <productname>
 */

if (version_compare(phpversion(), '7.1.0', '<')) {
    exit('PHP version is not high enough!. It must be at least 7.1.0');
}

chdir(dirname(__FILE__));
require_once 'vendor/autoload.php';

try {
    $entityManager = \Application\DocumentManagerFactory::getEntityManager(
        'pdo_mysql',
        'localhost',
        'root',
        'gamepointtrialzday',
        'gamepoint',
        'src/Domain/Entities',
        'cache/MySQL'
    );

    $requestData  = array();


    $orderDetailsQuery = new \Infrastructure\Queries\MySQL\ShoppingCart\MySQLOrderDetailsQuery($entityManager);
    $getAllPaymentsUseCase = new \Application\UseCases\GetOrderDetailsUseCase($orderDetailsQuery, $requestData);
    $result = $getAllPaymentsUseCase->invoke();

    echo '<pre>';
    print_r($result);
    echo '</pre>';
}catch(Exception $e){
    echo '<span style="color:red; font-weight: bold;">Error occur:</span> ' . $e->getMessage();
}