<?php
/**
 * Assignment #2
 * These are your database (MySQL) login credentials
 *    Host: localhost
 *    User: root
 *    Password: gamepointtrialzday
 *    Database: gamepoint
 *
 * These are the tables structures of the tables:
 *
 * CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `status` enum('forwarded','pending','authorised') DEFAULT NULL,
  `totalPrice` int(11) DEFAULT NULL,
  `methodID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
 *
 *  CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
 *
 *
 *
 *
 * Make a backend script, select all payments for user "Henk"
 * Output should be something like this:
 * <payment.id>   <user.firstname> <user.lastname>    <payment.totalPrice>     <payment.status>    <payment.methodID>
 *
 *
 */


if (version_compare(phpversion(), '7.1.0', '<')) {
    exit('PHP version is not high enough!. It must be at least 7.1.0');
}


chdir(dirname(__FILE__));
require_once 'vendor/autoload.php';

try {
    $entityManager = \Application\DocumentManagerFactory::getEntityManager(
        'pdo_mysql',
        'localhost',
        'root',
        'gamepointtrialzday',
        'gamepoint',
        'src/Domain/Entities',
        'cache/MySQL'
    );

    $requestData  = array();
    $requestData['user_name'] = $_GET['user_name'] ?? 'Henk';

    $paymentByUserNameQuery = new \Infrastructure\Queries\MySQL\Payment\MySQLPaymentsByUserNameQuery($entityManager);
    $getPaymentsByUserNameUseCase = new \Application\UseCases\GetPaymentsByUserNameUseCase($paymentByUserNameQuery, $requestData);
    $result = $getPaymentsByUserNameUseCase->invoke();

    echo '<pre>';
    print_r($result);
    echo '</pre>';
}catch(Exception $e){
    echo '<span style="color:red; font-weight: bold;">Error occur:</span> ' . $e->getMessage();
}
